use thiserror::Error;

#[derive(Error, Debug)]
pub enum DatabaseError {
    #[error("Error connecting to the database")]
    Open,
    #[error("Invalid data directory")]
    InvalidPath,
    #[error("Error querying data")]
    Query(#[from] diesel::result::Error),
    #[error("Error migrating db schema")]
    Migration,
    #[error("Pool Error")]
    Pool(#[from] diesel::r2d2::PoolError),
    #[error("IO error")]
    IO(#[from] std::io::Error),
    #[error("Unknown Error")]
    Unknown,
}
