use thiserror::Error;

pub use crate::database::DatabaseError;
pub use crate::feed_api::FeedApiError;
pub use crate::opml::OpmlError;
pub use crate::util::favicon_cache::FavIconError;
pub use crate::util::feed_parser::FeedParserError;

#[derive(Error, Debug)]
pub enum NewsFlashError {
    #[error("Database Error")]
    Database(#[from] DatabaseError),
    #[error("Feed API Error")]
    API(#[from] FeedApiError),
    #[error("IO Error")]
    IO(#[from] std::io::Error),
    #[error("Failed to load Feed API")]
    LoadBackend,
    #[error("Error laoding favicon")]
    Icon(#[from] FavIconError),
    #[error("This operation requires the api to be logged in")]
    NotLoggedIn,
    #[error("Error laoding/generating thumbnail")]
    Thumbnail,
    #[error("Parsing OPML file failed")]
    OPML(#[from] OpmlError),
    #[error("Failed to download images for article")]
    ImageDownload,
    #[error("Failed to download download full content for article")]
    GrabContent,
    #[error("Operation not possible during sync")]
    Syncing,
    #[error("An unknown Error occoured")]
    Unknown,
}
