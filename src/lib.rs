#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

mod action_cache;
mod database;
mod default_portal;
pub mod error;
pub mod feed_api;
mod feed_api_implementations;
pub mod models;
mod password_encryption;
mod schema;
pub mod util;

use crate::database::Database;
use crate::default_portal::DefaultPortal;
use chrono::{Duration, Utc};
use models::{ApiSecret, CategoryMapping, Thumbnail};
use parking_lot::{Mutex, RwLock};
use std::sync::Arc;

use crate::action_cache::ActionCache;
use crate::error::NewsFlashError;
use crate::feed_api::{FeedApi, Portal};
use crate::models::{
    Article, ArticleFilter, ArticleID, Category, CategoryID, CategoryType, Config, DatabaseSize, Enclosure, FatArticle, FavIcon, Feed, FeedID,
    FeedMapping, LoginData, Marked, PluginCapabilities, PluginID, PluginInfo, Read, Tag, TagID, Tagging, Url, NEWSFLASH_TOPLEVEL,
};
use crate::util::favicon_cache::FavIconCache;
pub use crate::util::feed_parser::{self, ParsedUrl};
use crate::util::html2text::Html2Text;
use crate::util::opml;
use article_scraper::ArticleScraper;
use feed_api_implementations::FeedApiImplementations;
use log::{error, info};
use once_cell::sync::Lazy;
use reqwest::Client;
use std::collections::hash_map::HashMap;
use std::path::Path;

static SCRAPER_DATA_DIR: &str = "scraper_data";
type NewsFlashResult<T> = Result<T, NewsFlashError>;

pub struct NewsFlash {
    db: Arc<Database>,
    api: RwLock<Box<dyn FeedApi>>,
    config: RwLock<Config>,
    icons: FavIconCache,
    scraper: ArticleScraper,
    sync_cache: Mutex<ActionCache>,
    sync_ongoing: Arc<RwLock<bool>>,
}

impl NewsFlash {
    pub fn list_backends() -> HashMap<PluginID, PluginInfo> {
        let mut map: HashMap<PluginID, PluginInfo> = HashMap::new();
        for api_meta in FeedApiImplementations::list() {
            map.insert(api_meta.id(), api_meta.info().unwrap());
        }
        map
    }

    pub fn new(data_dir: &Path, config_dir: &Path, id: &PluginID, user_api_secret: Option<ApiSecret>) -> NewsFlashResult<Self> {
        // create data dir if it doesn't already exist
        std::fs::DirBuilder::new().recursive(true).create(data_dir)?;
        std::fs::DirBuilder::new().recursive(true).create(config_dir)?;

        let db = Database::new(data_dir)?;
        let db = Arc::new(db);
        let api = NewsFlash::load_backend(id, config_dir, db.clone(), user_api_secret)?;
        let icons = FavIconCache::new(&db)?;
        let scraper = ArticleScraper::new(data_dir.join(SCRAPER_DATA_DIR));
        let config = Config::open(config_dir)?;
        let sync_cache = Mutex::new(ActionCache::new());
        let base = NewsFlash {
            db,
            api: RwLock::new(api),
            config: RwLock::new(config),
            icons,
            scraper,
            sync_cache,
            sync_ongoing: Arc::new(RwLock::new(false)),
        };

        Ok(base)
    }

    pub fn try_load(data_dir: &Path, config_dir: &Path) -> NewsFlashResult<Self> {
        let config = Config::open(config_dir)?;
        let plugin_id = config.get_backend().ok_or(NewsFlashError::LoadBackend)?;
        Self::new(data_dir, config_dir, &plugin_id, None)
    }

    fn load_backend(
        backend_id: &PluginID,
        data_dir: &Path,
        db: Arc<Database>,
        user_api_secret: Option<ApiSecret>,
    ) -> NewsFlashResult<Box<dyn FeedApi>> {
        info!("Loading backend {}", backend_id);
        if let Some(meta_data) = FeedApiImplementations::get(backend_id) {
            let portal = NewsFlash::default_portal(db)?;
            let backend = meta_data.get_instance(data_dir, portal, user_api_secret)?;
            return Ok(backend);
        } else {
            error!("No meta object for id '{}' found", backend_id);
        }
        Err(NewsFlashError::LoadBackend)
    }

    pub fn id(&self) -> Option<PluginID> {
        self.config.read().get_backend()
    }

    pub fn user_name(&self) -> Option<String> {
        self.api.read().user_name()
    }

    pub fn features(&self) -> NewsFlashResult<PluginCapabilities> {
        let features = self.api.read().features()?;
        Ok(features)
    }

    pub fn get_login_data(&self) -> Option<LoginData> {
        self.api.read().get_login_data()
    }

    pub fn is_sync_ongoing(&self) -> bool {
        *self.sync_ongoing.read()
    }

    pub fn is_database_empty(&self) -> NewsFlashResult<bool> {
        let is_empty = self.db.is_empty()?;
        Ok(is_empty)
    }

    fn default_portal(db: Arc<Database>) -> NewsFlashResult<Box<dyn Portal>> {
        let portal = DefaultPortal::new(db);
        let portal = Box::new(portal);
        Ok(portal)
    }

    pub fn set_keep_articles_duration(&self, keep_articles: Option<chrono::Duration>) -> NewsFlashResult<()> {
        self.config.write().set_keep_articles_duration(keep_articles)?;
        if let Some(keep_articles) = keep_articles {
            self.db.drop_old_articles(keep_articles)?;
        }

        Ok(())
    }

    pub fn get_keep_articles_duration(&self) -> Option<chrono::Duration> {
        self.config.read().get_keep_articles_duration()
    }

    pub fn error_login_related(error: &NewsFlashError) -> bool {
        match error {
            NewsFlashError::LoadBackend | NewsFlashError::NotLoggedIn | NewsFlashError::API(_) => true,

            NewsFlashError::Database(_)
            | NewsFlashError::GrabContent
            | NewsFlashError::Icon(_)
            | NewsFlashError::Thumbnail
            | NewsFlashError::ImageDownload
            | NewsFlashError::IO(_)
            | NewsFlashError::OPML(_)
            | NewsFlashError::Unknown
            | NewsFlashError::Syncing => false,
        }
    }

    pub async fn get_icon_info(&self, feed_id: &FeedID, client: &Lazy<Client>) -> NewsFlashResult<FavIcon> {
        let info = self.icons.get_icon(feed_id, &self.api, client).await?;
        Ok(info)
    }

    pub async fn get_article_thumbnail(&self, article_id: &ArticleID, client: &Lazy<Client>) -> NewsFlashResult<Thumbnail> {
        if let Ok(thumbnail) = self.db.read_thumbnail(article_id) {
            if thumbnail.data.is_some() {
                return Ok(thumbnail);
            }

            if Utc::now().naive_utc() - thumbnail.last_try > Duration::days(4) {
                self.download_thumbnail(article_id, client).await?;
            } else {
                log::debug!(
                    "Tried to download thumbnail for '{}' recently, will not attemt to download again.",
                    article_id
                );
            }
        }

        self.download_thumbnail(article_id, client).await
    }

    async fn download_thumbnail(&self, article_id: &ArticleID, client: &Lazy<Client>) -> NewsFlashResult<Thumbnail> {
        if let Ok(article) = self.db.read_article(article_id) {
            if let Some(thumb_url) = article.thumbnail_url {
                if let Ok(thumbnail) = Thumbnail::from_url(&thumb_url, article_id, client).await {
                    self.db.insert_thumbnail(&thumbnail)?;
                    return Ok(thumbnail);
                } else {
                    log::warn!("downloading thumbnail '{}' failed", thumb_url);
                }
            } else {
                log::debug!("Couldn't download thumbnail: article '{}' doesn't specify thumbnail url", article_id);
            }
        } else {
            log::warn!("Couldn't download thumbnail: article with ID '{}' not found", article_id);
        }

        Err(NewsFlashError::Thumbnail)
    }

    pub fn database_size(&self) -> NewsFlashResult<DatabaseSize> {
        let size = self.db.size()?;
        Ok(size)
    }

    pub async fn login(&self, data: LoginData, client: &Client) -> NewsFlashResult<()> {
        let id = data.id();
        self.api.write().login(data, client).await?;
        self.config.write().set_backend(Some(&id))?;
        Ok(())
    }

    pub async fn logout(&self, client: &Client) -> NewsFlashResult<()> {
        self.config.write().set_backend(None)?;
        self.api.write().logout(client).await?;
        self.db.reset()?;
        Ok(())
    }

    pub async fn initial_sync(&self, client: &Client) -> NewsFlashResult<i64> {
        if self.api.read().is_logged_in(client).await? {
            *self.sync_ongoing.write() = true;

            let now = chrono::Utc::now();

            let result = self.api.read().initial_sync(client).await.map_err(|error| {
                *self.sync_ongoing.write() = false;
                error
            })?;
            // Filter out old articles that would be deleted right away afterwards
            let result = result.remove_old_articles(self.config.read().get_keep_articles_duration());
            let result = result.generate_tag_colors(&[]);
            // Modify result with all changes that happened during sync
            let result = self.sync_cache.lock().process_sync_result(result);
            // push all changes that happend druing sync to the backend
            self.sync_cache
                .lock()
                .execute_api_actions(&self.api, &self.config, client)
                .await
                .map_err(|error| {
                    *self.sync_ongoing.write() = false;
                    error
                })?;
            // reset the sync_cache for next sync
            self.sync_cache.lock().reset();

            let new_article_count = self
                .db
                .write_sync_result(result, self.config.read().get_keep_articles_duration())
                .map_err(|error| {
                    *self.sync_ongoing.write() = false;
                    error
                })?;

            self.config.write().set_last_sync(now).map_err(|error| {
                *self.sync_ongoing.write() = false;
                error
            })?;

            *self.sync_ongoing.write() = false;
            return Ok(new_article_count);
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn sync(&self, client: &Client) -> NewsFlashResult<i64> {
        if self.api.read().is_logged_in(client).await? {
            *self.sync_ongoing.write() = true;

            let now = chrono::Utc::now();
            let max_count = self.config.read().get_sync_amount();
            let last_sync = self.config.read().get_last_sync();

            let result = self.api.read().sync(max_count, last_sync, client).await.map_err(|error| {
                *self.sync_ongoing.write() = false;
                error
            })?;
            // Filter out old articles that would be deleted right away afterwards
            let result = result.remove_old_articles(self.config.read().get_keep_articles_duration());

            let tags = self.db.read_tags()?;
            let result = result.generate_tag_colors(&tags);
            // Modify result with all changes that happened during sync
            let result = self.sync_cache.lock().process_sync_result(result);
            // push all changes that happend druing sync to the backend
            self.sync_cache
                .lock()
                .execute_api_actions(&self.api, &self.config, client)
                .await
                .map_err(|error| {
                    *self.sync_ongoing.write() = false;
                    error
                })?;
            // reset the sync_cache for next sync
            self.sync_cache.lock().reset();

            let new_article_count = self
                .db
                .write_sync_result(result, self.config.read().get_keep_articles_duration())
                .map_err(|error| {
                    *self.sync_ongoing.write() = false;
                    error
                })?;
            self.config.write().set_last_sync(now).map_err(|error| {
                *self.sync_ongoing.write() = false;
                error
            })?;

            *self.sync_ongoing.write() = false;
            return Ok(new_article_count);
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn set_article_read(&self, articles: &[ArticleID], read: Read, client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                for article_id in articles {
                    match read {
                        Read::Read => self.sync_cache.lock().add_article_marked_read(article_id),
                        Read::Unread => self.sync_cache.lock().add_article_marked_unread(article_id),
                    }
                }
                self.db.set_article_read(articles, read)?;
            } else {
                let articles_before = self.db.read_articles(ArticleFilter::read_ids(articles.into(), read.invert()))?;
                self.db.set_article_read(articles, read)?;

                let api_result = self.api.read().set_article_read(articles, read, client).await;

                // in case of error, reset read state to what it was before
                if api_result.is_err() {
                    let ids_before = articles_before.iter().map(|a| a.article_id.clone()).collect::<Vec<_>>();
                    self.db.set_article_read(&ids_before, read.invert())?;
                }

                api_result?;
            }

            Ok(())
        } else {
            Err(NewsFlashError::NotLoggedIn)
        }
    }

    pub async fn set_article_marked(&self, articles: &[ArticleID], marked: Marked, client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                for article_id in articles {
                    match marked {
                        Marked::Marked => self.sync_cache.lock().add_article_mark(article_id),
                        Marked::Unmarked => self.sync_cache.lock().add_article_unmark(article_id),
                    }
                }

                self.db.set_article_marked(articles, marked)?;
            } else {
                let articles_before = self.db.read_articles(ArticleFilter::marked_ids(articles.into(), marked.invert()))?;
                self.db.set_article_marked(articles, marked)?;

                let article_ids_to_update = articles_before.iter().map(|a| &a.article_id).cloned().collect::<Vec<_>>();
                let api_result = self.api.read().set_article_marked(&article_ids_to_update, marked, client).await;

                // in case of error, reset marked state to what it was before
                if api_result.is_err() {
                    let ids_before = articles_before.iter().map(|a| a.article_id.clone()).collect::<Vec<_>>();
                    self.db.set_article_marked(&ids_before, marked.invert())?;
                }

                api_result?;
            }

            Ok(())
        } else {
            Err(NewsFlashError::NotLoggedIn)
        }
    }

    pub async fn set_feed_read(&self, feeds: &[FeedID], client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                for feed_id in feeds {
                    self.sync_cache.lock().add_feed_mark_read(feed_id);
                }
                self.db.set_feed_read(feeds)?;
            } else {
                let mut unread_articles_before: Vec<ArticleID> = Vec::new();
                for feed_id in feeds {
                    let articles_before = self.db.read_articles(ArticleFilter::feed_unread(feed_id))?;
                    unread_articles_before.append(&mut articles_before.into_iter().map(|a| a.article_id).collect());
                }

                self.db.set_feed_read(feeds)?;

                let last_sync = self.config.read().get_last_sync();
                let api_result = self.api.read().set_feed_read(feeds, &unread_articles_before, last_sync, client).await;

                // in case of error, reset read state to what it was before
                if api_result.is_err() {
                    self.db.set_article_read(&unread_articles_before, Read::Unread)?;
                }

                api_result?;
            }

            Ok(())
        } else {
            Err(NewsFlashError::NotLoggedIn)
        }
    }

    pub async fn set_category_read(&self, categories: &[CategoryID], client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                for category_id in categories {
                    self.sync_cache.lock().add_category_mark_read(category_id);
                }
                self.db.set_category_read(categories)?;
            } else {
                let mut unread_articles_before: Vec<ArticleID> = Vec::new();
                for category_id in categories {
                    let articles_before = self.db.read_articles(ArticleFilter::category_unread(category_id))?;
                    unread_articles_before.append(&mut articles_before.into_iter().map(|a| a.article_id).collect());
                }

                self.db.set_category_read(categories)?;
                let last_sync = self.config.read().get_last_sync();
                let api_result = self
                    .api
                    .read()
                    .set_category_read(categories, &unread_articles_before, last_sync, client)
                    .await;

                // in case of error, reset read state to what it was before
                if api_result.is_err() {
                    self.db.set_article_read(&unread_articles_before, Read::Unread)?;
                }

                api_result?;
            }

            Ok(())
        } else {
            Err(NewsFlashError::NotLoggedIn)
        }
    }

    pub async fn set_tag_read(&self, tags: &[TagID], client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                for tag_id in tags {
                    self.sync_cache.lock().add_tag_mark_read(tag_id);
                }
                self.db.set_tag_read(tags)?;
            } else {
                let mut unread_articles_before: Vec<ArticleID> = Vec::new();
                for tag_id in tags {
                    let articles_before = self.db.read_articles(ArticleFilter::tag_unread(tag_id))?;
                    unread_articles_before.append(&mut articles_before.into_iter().map(|a| a.article_id).collect());
                }

                self.db.set_tag_read(tags)?;
                let last_sync = self.config.read().get_last_sync();
                let api_result = self.api.read().set_tag_read(tags, &unread_articles_before, last_sync, client).await;

                // in case of error, reset read state to what it was before
                if api_result.is_err() {
                    self.db.set_article_read(&unread_articles_before, Read::Unread)?;
                }

                api_result?;
            }

            Ok(())
        } else {
            Err(NewsFlashError::NotLoggedIn)
        }
    }

    pub async fn set_all_read(&self, client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                let categories = self.db.read_categories()?;
                for category in categories {
                    self.sync_cache.lock().add_category_mark_read(&category.category_id);
                }
                self.db.set_all_read()?;
            } else {
                let unread_articles_before = self
                    .db
                    .read_articles(ArticleFilter::all_unread())?
                    .into_iter()
                    .map(|a| a.article_id)
                    .collect::<Vec<_>>();
                self.db.set_all_read()?;

                let last_sync = self.config.read().get_last_sync();
                let api_result = self.api.read().set_all_read(&unread_articles_before, last_sync, client).await;

                // in case of error, reset read state to what it was before
                if api_result.is_err() {
                    self.db.set_article_read(&unread_articles_before, Read::Unread)?;
                }

                api_result?;
            }

            Ok(())
        } else {
            Err(NewsFlashError::NotLoggedIn)
        }
    }

    pub async fn add_feed(
        &self,
        url: &Url,
        title: Option<String>,
        category_id: Option<CategoryID>,
        client: &Client,
    ) -> NewsFlashResult<(Feed, FeedMapping, Option<Category>, Option<CategoryMapping>)> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                return Err(NewsFlashError::Syncing);
            }

            let (feed, category) = self.api.read().add_feed(url, title, category_id.clone(), client).await?;

            self.db.insert_feed(&feed)?;

            let category_mapping = if let Some(category) = &category {
                let category_mapping = CategoryMapping {
                    parent_id: NEWSFLASH_TOPLEVEL.clone(),
                    category_id: category.category_id.clone(),
                    sort_index: None,
                };
                self.db.insert_category(category)?;
                self.db.insert_category_mapping(&category_mapping)?;
                Some(category_mapping)
            } else {
                None
            };

            let category_id = match category_id {
                Some(category_id) => Some(category_id),
                None => category.as_ref().map(|c| c.category_id.clone()),
            };

            let feed_mapping = FeedMapping {
                feed_id: feed.feed_id.clone(),
                category_id: category_id.unwrap_or(NEWSFLASH_TOPLEVEL.clone()),
                sort_index: None,
            };

            self.db.insert_feed_mapping(&feed_mapping)?;

            return Ok((feed, feed_mapping, category, category_mapping));
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn remove_feed(&self, feed: &Feed, client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                return Err(NewsFlashError::Syncing);
            }
            self.api.read().remove_feed(&feed.feed_id, client).await?;

            // remove feed from db
            self.db.drop_feed(&feed.feed_id)?;
            return Ok(());
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn move_feed(&self, from: &FeedMapping, to: &FeedMapping, client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                return Err(NewsFlashError::Syncing);
            }

            self.api
                .read()
                .move_feed(&from.feed_id, &from.category_id, &to.category_id, client)
                .await?;

            // drop mapping 'from'
            self.db.drop_feed_mapping(from)?;

            // add mapping 'to'
            self.db.insert_feed_mapping(to)?;
            return Ok(());
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn rename_feed(&self, feed: &Feed, new_title: &str, client: &Client) -> NewsFlashResult<Feed> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                return Err(NewsFlashError::Syncing);
            }

            let new_id = self.api.read().rename_feed(&feed.feed_id, new_title, client).await?;

            let mut modified_feed = feed.clone();
            modified_feed.label = new_title.to_owned();
            modified_feed.feed_id = new_id.clone();

            self.db.insert_feed(&modified_feed)?;

            if new_id != feed.feed_id {
                self.db.drop_feed(&feed.feed_id)?;

                // fix mappings
                let mappings = self.db.read_feed_mappings(Some(&feed.feed_id), None)?;
                let modified_mappings: Vec<FeedMapping> = mappings
                    .into_iter()
                    .map(|mut mapping| {
                        mapping.feed_id = new_id.clone();
                        mapping
                    })
                    .collect();
                self.db.drop_mapping_of_feed(&feed.feed_id)?;
                self.db.write_feed_mappings(&modified_mappings)?;

                // fix articles
                let articles = self.db.read_articles(ArticleFilter {
                    feeds: Some([feed.feed_id.clone()].into()),
                    ..ArticleFilter::default()
                })?;
                let mut modified_ids: Vec<ArticleID> = Vec::new();
                let modified_articles: Vec<Article> = articles
                    .into_iter()
                    .map(|mut article| {
                        modified_ids.push(article.article_id.clone());
                        article.feed_id = new_id.clone();
                        article
                    })
                    .collect();
                self.db.drop_articles(&modified_ids)?;
                self.db.write_articles(&modified_articles)?;
            }
            return Ok(modified_feed);
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn add_category(
        &self,
        title: &str,
        parent: Option<&CategoryID>,
        sort_index: Option<i32>,
        client: &Client,
    ) -> NewsFlashResult<(Category, CategoryMapping)> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                return Err(NewsFlashError::Syncing);
            }

            let category_id = self.api.read().add_category(title, parent, client).await?;

            let category = Category {
                category_id: category_id.clone(),
                label: title.to_owned(),
                category_type: CategoryType::Default,
            };

            let category_mapping = CategoryMapping {
                parent_id: match parent {
                    Some(parent) => parent.clone(),
                    None => NEWSFLASH_TOPLEVEL.clone(),
                },
                category_id,
                sort_index,
            };

            self.db.insert_category(&category)?;
            self.db.insert_category_mapping(&category_mapping)?;

            return Ok((category, category_mapping));
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn remove_category(&self, category: &Category, remove_children: bool, client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                return Err(NewsFlashError::Syncing);
            }

            self.api.read().remove_category(&category.category_id, remove_children, client).await?;
        } else {
            return Err(NewsFlashError::NotLoggedIn);
        }

        if remove_children {
            self.remove_category_from_db_recurse(category)?;
        } else {
            self.remove_category_from_db_move_children_up(category)?;
        }

        Ok(())
    }

    fn remove_category_from_db_move_children_up(&self, category: &Category) -> NewsFlashResult<()> {
        let parent_id = self
            .db
            .read_category_mappings(None, Some(&category.category_id))?
            .first()
            .map(|m| m.parent_id.clone())
            .unwrap_or_else(|| NEWSFLASH_TOPLEVEL.clone());

        // map feeds of category as children of parent
        let feed_mappings = self.db.read_feed_mappings(None, Some(&category.category_id))?;
        for mut mapping in feed_mappings {
            self.db.drop_feed_mapping(&mapping)?;
            mapping.category_id = parent_id.clone();
            self.db.insert_feed_mapping(&mapping)?;
        }

        // map child categories of category as children of parent
        let category_mappings = self.db.read_category_mappings(None, Some(&category.category_id))?;
        for mut mapping in category_mappings {
            self.db.drop_category_mapping(&mapping)?;
            mapping.parent_id = parent_id.clone();
            self.db.insert_category_mapping(&mapping)?;
        }

        Ok(())
    }

    fn remove_category_from_db_recurse(&self, category: &Category) -> NewsFlashResult<()> {
        // remove childen feeds
        let mappings = self.db.read_feed_mappings(None, Some(&category.category_id))?;
        for mapping in mappings {
            self.db.drop_feed(&mapping.feed_id)?;
        }

        // remove category
        self.db.drop_category(category)?;

        // look for children categories and recurse
        let mappings = self.db.read_category_mappings(None, Some(&category.category_id))?;
        let categories = self.db.read_categories()?;

        let child_categories: Vec<Category> = categories
            .into_iter()
            .filter(|category| mappings.iter().any(|mapping| mapping.category_id == category.category_id))
            .collect();
        for child_category in child_categories {
            self.remove_category_from_db_recurse(&child_category)?;
        }

        Ok(())
    }

    pub async fn rename_category(&self, category: &Category, new_title: &str, client: &Client) -> NewsFlashResult<Category> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                return Err(NewsFlashError::Syncing);
            }

            let mut modified_category_mappings = self.db.read_category_mappings(None, Some(&category.category_id))?;
            let mut modified_feed_mappings = self.db.read_feed_mappings(None, Some(&category.category_id))?;

            let new_id = self.api.read().rename_category(&category.category_id, new_title, client).await?;

            let mut modified_category = category.clone();
            modified_category.label = new_title.to_owned();

            if new_id != category.category_id {
                self.db.drop_category(category)?;
                self.db.drop_feed_mappings_of_category(&category.category_id)?;
                modified_category.category_id = new_id.clone();

                // fix mappings
                modified_feed_mappings = modified_feed_mappings
                    .into_iter()
                    .map(|mut mapping| {
                        mapping.category_id = new_id.clone();
                        mapping
                    })
                    .collect();

                modified_category_mappings = modified_category_mappings
                    .into_iter()
                    .map(|mut m| {
                        m.category_id = new_id.clone();
                        m
                    })
                    .collect();
            }

            self.db.insert_category(&modified_category)?;
            self.db.insert_category_mappings(&modified_category_mappings)?;
            self.db.insert_feed_mappings(&modified_feed_mappings)?;
            return Ok(modified_category);
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn move_category(&self, category_id: &CategoryID, parent: &CategoryID, client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                return Err(NewsFlashError::Syncing);
            }

            self.api.read().move_category(category_id, parent, client).await?;

            let category_mapping = CategoryMapping {
                parent_id: parent.clone(),
                category_id: category_id.clone(),
                sort_index: None,
            };

            self.db.drop_feed_mappings_of_category(category_id)?;
            self.db.insert_category_mapping(&category_mapping)?;
            return Ok(());
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn add_tag(&self, title: &str, color: Option<String>, sort_index: Option<i32>, client: &Client) -> NewsFlashResult<Tag> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                return Err(NewsFlashError::Syncing);
            }

            let tag_id = self.api.read().add_tag(title, client).await?;
            let tag = Tag {
                tag_id,
                label: title.to_owned(),
                color,
                sort_index,
            };

            self.db.insert_tag(&tag)?;
            return Ok(tag);
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn remove_tag(&self, tag: &Tag, client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                return Err(NewsFlashError::Syncing);
            }

            self.api.read().remove_tag(&tag.tag_id, client).await?;

            self.db.drop_tag(tag)?;
            return Ok(());
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn edit_tag(&self, tag: &Tag, new_title: &str, new_color: &Option<String>, client: &Client) -> NewsFlashResult<Tag> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                return Err(NewsFlashError::Syncing);
            }

            let new_id = self.api.read().rename_tag(&tag.tag_id, new_title, client).await?;

            let taggings = self.db.read_taggings(None, Some(&tag.tag_id))?;
            self.db.drop_tag(tag)?;
            let mutated_tag = Tag {
                tag_id: new_id.clone(),
                label: new_title.to_owned(),
                color: new_color.clone(),
                sort_index: tag.sort_index,
            };
            self.db.insert_tag(&mutated_tag)?;
            self.db.insert_taggings(&taggings)?;

            let taggings = if new_id != tag.tag_id {
                taggings
                    .into_iter()
                    .map(|mut tagging| {
                        tagging.tag_id = new_id.clone();
                        tagging
                    })
                    .collect()
            } else {
                taggings
            };
            self.db.insert_taggings(&taggings)?;

            return Ok(mutated_tag);
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn tag_article(&self, article: &Article, tag: &Tag, client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                self.sync_cache.lock().add_article_tagged(&article.article_id, &tag.tag_id);
            } else {
                self.api.read().tag_article(&article.article_id, &tag.tag_id, client).await?;
                let tagging = Tagging {
                    article_id: article.article_id.clone(),
                    tag_id: tag.tag_id.clone(),
                };

                self.db.insert_tagging(&tagging)?;
            }
            return Ok(());
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn untag_article(&self, article: &Article, tag: &Tag, client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                self.sync_cache.lock().add_article_untagged(&article.article_id, &tag.tag_id);
            } else {
                self.api.read().untag_article(&article.article_id, &tag.tag_id, client).await?;
                let tagging = Tagging {
                    article_id: article.article_id.clone(),
                    tag_id: tag.tag_id.clone(),
                };

                self.db.drop_tagging(&tagging)?;
            }
            return Ok(());
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub async fn import_opml(&self, opml: &str, parse_all_feeds: bool, client: &Client) -> NewsFlashResult<()> {
        if self.api.read().is_logged_in(client).await? {
            if *self.sync_ongoing.read() {
                return Err(NewsFlashError::Syncing);
            }

            self.api.read().import_opml(opml, client).await?;
            let opml_result = opml::parse_opml(opml, parse_all_feeds, client).await?;

            self.db.insert_categories(&opml_result.categories)?;
            self.db.insert_feeds(&opml_result.feeds)?;
            self.db.insert_feed_mappings(&opml_result.feed_mappings)?;
            self.db.insert_category_mappings(&opml_result.category_mappings)?;
            return Ok(());
        }
        Err(NewsFlashError::NotLoggedIn)
    }

    pub fn export_opml(&self) -> NewsFlashResult<String> {
        if *self.sync_ongoing.read() {
            return Err(NewsFlashError::Syncing);
        }

        let categories = self.db.read_categories()?;
        let category_mappings = self.db.read_category_mappings(None, None)?;
        let feeds = self.db.read_feeds()?;
        let feed_mappings = self.db.read_feed_mappings(None, None)?;

        let opml_string = opml::generate_opml(&categories, &category_mappings, &feeds, &feed_mappings)?;
        Ok(opml_string)
    }

    pub fn get_categories(&self) -> NewsFlashResult<(Vec<Category>, Vec<CategoryMapping>)> {
        let categories = self.db.read_categories()?;
        let category_mappings = self.db.read_category_mappings(None, None)?;
        Ok((categories, category_mappings))
    }

    pub fn unread_count_category(&self, category: &CategoryID) -> NewsFlashResult<i64> {
        let count = self.db.unread_count_category(category)?;
        Ok(count)
    }

    pub fn marked_count_category(&self, category: &CategoryID) -> NewsFlashResult<i64> {
        let count = self.db.marked_count_category(category)?;
        Ok(count)
    }

    pub fn get_feeds(&self) -> NewsFlashResult<(Vec<Feed>, Vec<FeedMapping>)> {
        let feeds = self.db.read_feeds()?;
        let mappings = self.db.read_feed_mappings(None, None)?;
        Ok((feeds, mappings))
    }

    pub fn unread_count_feed(&self, feed: &FeedID) -> NewsFlashResult<i64> {
        let count = self.db.unread_count_feed(feed)?;
        Ok(count)
    }

    pub fn marked_count_feed(&self, feed: &FeedID) -> NewsFlashResult<i64> {
        let count = self.db.marked_count_feed(feed)?;
        Ok(count)
    }

    pub fn unread_count_feed_map(&self, exclude_future: bool) -> NewsFlashResult<HashMap<FeedID, i64>> {
        let mut count_vec = self.db.unread_count_feed_map(exclude_future)?;
        let mut map: HashMap<FeedID, i64> = HashMap::new();
        count_vec.drain(..).for_each(|c| {
            map.insert(c.feed_id, c.count);
        });
        Ok(map)
    }

    pub fn marked_count_feed_map(&self) -> NewsFlashResult<HashMap<FeedID, i64>> {
        let mut count_vec = self.db.marked_count_feed_map()?;
        let mut map: HashMap<FeedID, i64> = HashMap::new();
        count_vec.drain(..).for_each(|c| {
            map.insert(c.feed_id, c.count);
        });
        Ok(map)
    }

    pub fn get_tags(&self) -> NewsFlashResult<(Vec<Tag>, Vec<Tagging>)> {
        let tags = self.db.read_tags()?;
        let taggings = self.db.read_taggings(None, None)?;
        Ok((tags, taggings))
    }

    pub fn get_tags_of_article(&self, article_id: &ArticleID) -> NewsFlashResult<Vec<Tag>> {
        let tags = self.db.read_tags_for_article(article_id)?;
        Ok(tags)
    }

    pub fn unread_count_tag(&self, tag: &TagID) -> NewsFlashResult<i64> {
        let count = self.db.unread_count_tag(tag)?;
        Ok(count)
    }

    pub fn marked_count_tag(&self, tag: &TagID) -> NewsFlashResult<i64> {
        let count = self.db.marked_count_tag(tag)?;
        Ok(count)
    }

    pub fn unread_count_all(&self) -> NewsFlashResult<i64> {
        let count = self.db.unread_count_all()?;
        Ok(count)
    }

    pub fn marked_count_all(&self) -> NewsFlashResult<i64> {
        let count = self.db.marked_count_all()?;
        Ok(count)
    }

    pub fn get_articles(&self, filter: ArticleFilter) -> NewsFlashResult<Vec<Article>> {
        let articles = self.db.read_articles(filter)?;
        Ok(articles)
    }

    pub fn get_article(&self, id: &ArticleID) -> NewsFlashResult<Article> {
        let article = self.db.read_article(id)?;
        Ok(article)
    }

    pub fn get_fat_articles(&self, filter: ArticleFilter) -> NewsFlashResult<Vec<FatArticle>> {
        let articles = self.db.read_fat_articles(filter)?;
        Ok(articles)
    }

    pub fn get_fat_article(&self, id: &ArticleID) -> NewsFlashResult<FatArticle> {
        let article = self.db.read_fat_article(id)?;
        Ok(article)
    }

    pub fn get_enclosures(&self, id: &ArticleID) -> NewsFlashResult<Vec<Enclosure>> {
        let enclosures = self.db.read_enclosures(id)?;
        Ok(enclosures)
    }

    pub async fn article_download_images(&self, id: &ArticleID, client: &Client) -> NewsFlashResult<FatArticle> {
        let mut article = self.get_fat_article(id)?;

        if let Some(scraped_content) = article.scraped_content {
            let processed_scraped_content = self
                .scraper
                .image_downloader
                .download_images_from_string(&scraped_content, client)
                .await
                .map_err(|_| NewsFlashError::ImageDownload)?;
            article.scraped_content = Some(processed_scraped_content);
        } else if let Some(html) = article.html {
            let processed_html = self
                .scraper
                .image_downloader
                .download_images_from_string(&html, client)
                .await
                .map_err(|_| NewsFlashError::ImageDownload)?;
            article.html = Some(processed_html);
        }

        self.db.update_article_grabbed_content(&article)?;

        Ok(article)
    }

    pub async fn article_scrap_content(&self, id: &ArticleID, client: &Client) -> NewsFlashResult<FatArticle> {
        let mut article = self.get_fat_article(id)?;

        if let Some(url) = &article.url {
            let processed_article = self.scraper.parse(url, false, client).await.map_err(|e| {
                error!("Internal scraper: '{}' ({})", e, url);
                NewsFlashError::GrabContent
            })?;

            info!("Internal scraper: successfully scraped: '{}'", url);
            if let Some(html) = processed_article.html {
                article.plain_text = Html2Text::process(&html);
                article.scraped_content = Some(html);
            }
            if let Some(title) = processed_article.title {
                if article.title.is_none() {
                    article.title = Some(title);
                }
            }
            if let Some(author) = processed_article.author {
                if article.author.is_none() {
                    article.author = Some(author);
                }
            }

            self.db.update_article_grabbed_content(&article)?;
            Ok(article)
        } else {
            error!("Article doesn't contain source URL");
            Err(NewsFlashError::GrabContent)
        }
    }

    pub fn update_external_scraped_content(&self, article: &FatArticle) -> NewsFlashResult<()> {
        self.db.update_article_grabbed_content(article)?;
        Ok(())
    }
}
