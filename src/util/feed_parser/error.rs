use thiserror::Error;

#[derive(Error, Debug)]
pub enum FeedParserError {
    #[error("Http request failed")]
    Http(#[from] reqwest::Error),
    #[error("Failed to parse feed url from HTML")]
    Html,
    #[error("Failed to parse feed")]
    Feed,
}
