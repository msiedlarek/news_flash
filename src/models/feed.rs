use crate::models::{CategoryID, FeedID, Url};
use crate::schema::{feed_mapping, feeds};
use std::hash::{Hash, Hasher};

#[derive(Identifiable, Clone, Insertable, Queryable, Eq, Debug)]
#[diesel(primary_key(feed_id))]
#[diesel(table_name = feeds)]
pub struct Feed {
    pub feed_id: FeedID,
    pub label: String,
    pub website: Option<Url>,
    pub feed_url: Option<Url>,
    pub icon_url: Option<Url>,
}

impl Hash for Feed {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.feed_id.hash(state);
    }
}

impl PartialEq for Feed {
    fn eq(&self, other: &Feed) -> bool {
        self.feed_id == other.feed_id
    }
}

//------------------------------------------------------------------

#[derive(Queryable, Debug)]
pub struct FeedCount {
    pub feed_id: FeedID,
    pub count: i64,
}

//------------------------------------------------------------------

#[derive(Identifiable, Insertable, Associations, Queryable, Eq, PartialEq, Debug, Hash, Clone)]
#[diesel(primary_key(feed_id))]
#[diesel(table_name = feed_mapping)]
#[diesel(belongs_to(Feed, foreign_key = feed_id))]
pub struct FeedMapping {
    pub feed_id: FeedID,
    pub category_id: CategoryID,
    pub sort_index: Option<i32>,
}
