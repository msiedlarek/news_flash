#[derive(Debug)]
pub struct DatabaseSize {
    pub allocated: u64,
    pub on_disk: u64,
}
