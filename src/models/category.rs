use crate::models::CategoryID;
use crate::schema::{categories, category_mapping};
use diesel::backend::RawValue;
use diesel::deserialize;
use diesel::deserialize::FromSql;
use diesel::serialize::{self, IsNull};
use diesel::serialize::{Output, ToSql};
use diesel::sql_types::Integer;
use diesel::sqlite::Sqlite;
use serde::{Deserialize, Serialize};
use std::hash::{Hash, Hasher};

#[derive(Identifiable, Clone, Insertable, Queryable, Eq, Debug)]
#[diesel(primary_key(category_id))]
#[diesel(table_name = categories)]
pub struct Category {
    pub category_id: CategoryID,
    pub label: String,
    pub category_type: CategoryType,
}

impl Hash for Category {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.category_id.hash(hasher);
    }
}

impl PartialEq for Category {
    fn eq(&self, other: &Category) -> bool {
        self.category_id == other.category_id
    }
}

//------------------------------------------------------------------

#[derive(PartialEq, Eq, Copy, Clone, Debug, AsExpression, FromSqlRow, Serialize, Deserialize)]
#[diesel(sql_type = Integer)]
pub enum CategoryType {
    Default,
    Generated,
}

impl CategoryType {
    pub fn to_int(self) -> i32 {
        self as i32
    }

    pub fn from_int(int: i32) -> Self {
        match int {
            0 => CategoryType::Default,
            1 => CategoryType::Generated,

            _ => CategoryType::Default,
        }
    }
}

impl FromSql<Integer, Sqlite> for CategoryType {
    fn from_sql(bytes: RawValue<Sqlite>) -> deserialize::Result<Self> {
        let int = i32::from_sql(bytes)?;
        Ok(CategoryType::from_int(int))
    }
}

impl ToSql<Integer, Sqlite> for CategoryType {
    fn to_sql<'b>(&'b self, out: &mut Output<'b, '_, Sqlite>) -> serialize::Result {
        out.set_value(*self as i32);
        Ok(IsNull::No)
    }
}

//------------------------------------------------------------------

#[derive(Identifiable, Clone, Insertable, Associations, Queryable, Eq, PartialEq, Debug)]
#[diesel(primary_key(category_id))]
#[diesel(table_name = category_mapping)]
#[diesel(belongs_to(Category, foreign_key = category_id))]
pub struct CategoryMapping {
    pub parent_id: CategoryID,
    pub category_id: CategoryID,
    pub sort_index: Option<i32>,
}
